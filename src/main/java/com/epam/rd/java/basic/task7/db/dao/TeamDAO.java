package com.epam.rd.java.basic.task7.db.dao;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.entity.Team;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class TeamDAO {
    private final String URL;

    public TeamDAO(String URL) {
        this.URL = URL;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO teams values (default,?)",
                     RETURN_GENERATED_KEYS)) {
            statement.setString(1, team.getName());
            executeUpdate(statement);
            setTeamId(team, statement);
            return true;
        } catch (SQLException exception) {
            throw new DBException(exception.getMessage());
        }
    }

    private void setTeamId(Team team, PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        if (generatedKeys.next()) {
            int id = generatedKeys.getInt(1);
            team.setId(id);
        }
    }

    private void executeUpdate(PreparedStatement statement) throws SQLException, DBException {
        int affectedRows = statement.executeUpdate();
        if (affectedRows == 0) {
            throw new DBException("Team not added");
        }
    }

    public List<Team> findAllTeams() throws DBException {
        try (Connection connection = DriverManager.getConnection(URL);
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM teams");
            return collectToList(resultSet);
        } catch (SQLException exception) {
            throw new DBException(exception.getMessage());
        }
    }

    private List<Team> collectToList(ResultSet resultSet) throws SQLException {
        List<Team> teamsList = new ArrayList<>();
        while (resultSet.next()) {
            Team team = parseRow(resultSet);
            teamsList.add(team);
        }
        return teamsList;
    }

    private Team parseRow(ResultSet resultSet) throws SQLException {
        Team team = new Team();
        team.setId(resultSet.getInt(1));
        team.setName(resultSet.getString(2));
        return team;
    }

    public Team getTeam(String name) throws DBException {
        try (Connection connection = DriverManager.getConnection(URL)) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM teams where name LIKE ?");
            statement.setString(1, "%" + name + "%");
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return parseRow(resultSet);
            }
            return null;
        } catch (SQLException exception) {
            throw new DBException(exception.getMessage());
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(URL)) {
            connection.setAutoCommit(false);
            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM teams WHERE id = ?")) {
                statement.setInt(1, team.getId());
                statement.executeUpdate();
            } catch (Exception e) {
                connection.rollback();
                throw new DBException("Transaction failed");
            }
            connection.commit();
            return true;
        } catch (SQLException exception) {
            throw new DBException(exception.getMessage());
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement statement = connection.prepareStatement("UPDATE teams SET name = ? WHERE id = ?")) {
            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            statement.executeUpdate();
            return true;
        } catch (SQLException exception) {
            throw new DBException(exception.getMessage());
        }
    }
}
