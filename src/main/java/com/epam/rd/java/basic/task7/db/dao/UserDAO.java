package com.epam.rd.java.basic.task7.db.dao;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class UserDAO {
    private final String URL;

    public UserDAO(String URL) {
        this.URL = URL;
    }

    public List<User> findAllUsers() throws DBException {
        try (Connection connection = DriverManager.getConnection(URL);
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users");
            return collectToList(resultSet);
        } catch (SQLException exception) {
            throw new DBException(exception.getMessage());
        }
    }

    private List<User> collectToList(ResultSet resultSet) throws SQLException {
        List<User> usersList = new ArrayList<>();
        while (resultSet.next()) {
            User user = parsRow(resultSet);
            usersList.add(user);
        }
        return usersList;
    }

    private User parsRow(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt(1));
        user.setLogin(resultSet.getString(2));
        return user;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO users values (default,?)",
                     RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getLogin());
            executeUpdate(statement);
            setUserId(user, statement);
            return true;
        } catch (SQLException exception) {
            throw new DBException(exception.getMessage());
        }
    }

    private void executeUpdate(PreparedStatement statement) throws SQLException, DBException {
        int affectedRows = statement.executeUpdate();
        if (affectedRows == 0) {
            throw new DBException("User was not created");
        }
    }

    private void setUserId(User user, PreparedStatement statement) throws SQLException, DBException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        if (generatedKeys.next()) {
            user.setId(generatedKeys.getInt(1));
        } else {
            throw new DBException("Can not obtain an account ID");
        }
    }

    public User getUser(String login) throws DBException {
        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE login LIKE ?")) {
            statement.setString(1, "%" + login + "%");
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return parsRow(resultSet);
            }
            return null;
        } catch (SQLException exception) {
            throw new DBException(exception.getMessage());
        }
    }

    public boolean deleteUsers(User[] users) throws DBException {
        try (Connection connection = DriverManager.getConnection(URL)) {
            for (User user : users) {
                deleteUser(connection, user);
            }
            return true;
        } catch (SQLException exception) {
            throw new DBException(exception.getMessage());
        }
    }

    private void deleteUser(Connection connection, User user) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE id = ?")) {
            statement.setInt(1, user.getId());
            statement.executeUpdate();
        }
    }
}
