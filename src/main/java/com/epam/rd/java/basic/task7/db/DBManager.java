package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.dao.TeamDAO;
import com.epam.rd.java.basic.task7.db.dao.UserDAO;
import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String URL = "jdbc:derby:memory:testdb";

	private static final DBManager instance = new DBManager();

	private final UserDAO userDAO;
	private final TeamDAO teamDAO;

	public static synchronized DBManager getInstance() {
		return instance;
	}

	private DBManager() {
		userDAO = new UserDAO(URL);
		teamDAO = new TeamDAO(URL);
	}

	public List<User> findAllUsers() throws DBException {
		return userDAO.findAllUsers();
	}

	public boolean insertUser(User user) throws DBException {
		return userDAO.insertUser(user);
	}

	public boolean deleteUsers(User... users) throws DBException {
		return userDAO.deleteUsers(users);
	}

	public User getUser(String login) throws DBException {
		return userDAO.getUser(login);
	}

	public Team getTeam(String name) throws DBException {
		return teamDAO.getTeam(name);
	}

	public List<Team> findAllTeams() throws DBException {
		return teamDAO.findAllTeams();
	}

	public boolean insertTeam(Team team) throws DBException {
		return teamDAO.insertTeam(team);
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		try (Connection connection = DriverManager.getConnection(URL)) {
			connection.setAutoCommit(false);
			for (Team team : teams) {
				try (PreparedStatement statement = connection.prepareStatement("INSERT INTO users_teams values (?,?)")) {
					statement.setInt(1, user.getId());
					statement.setInt(2, team.getId());
					statement.executeUpdate();
				} catch (Exception e) {
					connection.rollback();
					throw new DBException("Transaction failed");
				}
			}
			connection.commit();
			return true;
		} catch (SQLException exception) {
			throw new DBException(exception.getMessage());
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		String getUserTeams = "SELECT teams.id, teams.name\n" +
				"FROM\n" +
				"users_teams\n" +
				"INNER JOIN users ON users_teams.user_id = users.id\n" +
				"INNER JOIN teams ON users_teams.team_id = teams.id\n" +
				"WHERE users.id =?";
		try (Connection connection = DriverManager.getConnection(URL)) {
			PreparedStatement statement = connection.prepareStatement(getUserTeams);
			statement.setInt(1, user.getId());
			ResultSet resultSet = statement.executeQuery();
			return collectToListTeams(resultSet);
		} catch (SQLException exception) {
			throw new DBException(exception.getMessage());
		}
	}


	private List<Team> collectToListTeams(ResultSet resultSet) throws SQLException {
		List<Team> teamList = new ArrayList<>();
		while (resultSet.next()) {
			Team team = new Team();
			team.setId(resultSet.getInt(1));
			team.setName(resultSet.getString(2));
			teamList.add(team);
		}

		return teamList;
	}

	public boolean deleteTeam(Team team) throws DBException {
		return teamDAO.deleteTeam(team);
	}

	public boolean updateTeam(Team team) throws DBException {
		return teamDAO.updateTeam(team);
	}

}
